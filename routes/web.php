<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['register' => false]);

// Show dummy page just to make sure one's is logged in
Route::get('/', 'HomeController@index')->middleware('auth');

Route::group(['middleware' => ['AdminRoute']], function () {
    /*
    |  Route for company
    */

    // show company's page
    Route::get('/company', 'CompanyController@index');

    // To create company's page
    Route::get('/company/create', 'CompanyController@create');

    // Save created company's data
    Route::post('/company/create/save', 'CompanyController@createSave');

    // Show 1 company in the modal
    Route::get('/company/show/{id}', 'CompanyController@show');

    // Save edited company's data
    Route::post('/company/{id}/store', 'CompanyController@store');

    // Delete 1 company data
    Route::post('/company/{id}/delete', 'CompanyController@delete');

    // Show company data for datatables
    Route::get('/company/data', 'CompanyController@data');

    // Show company's list for employee's page
    Route::get('/company/list', 'CompanyController@list');

    /*
    |  Route for employee
    */

    // Show employee's page
    Route::get('/employee', 'EmployeeController@index');

    // To create employee's page
    Route::get('/employee/create', 'EmployeeController@create');

    // Save created employee's data
    Route::post('/employee/create/save', 'EmployeeController@createSave');

    // Show 1 employee in the modal
    Route::get('/employee/show/{id}', 'EmployeeController@show');

    // Delete 1 employee
    Route::post('/employee/{id}/delete', 'EmployeeController@delete');

    // Save edited employee data
    Route::post('/employee/{id}/store', 'EmployeeController@store');

    // Show all employee for datatables
    Route::get('/employee/data', 'EmployeeController@data');
});
