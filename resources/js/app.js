/*
|  Import scripts file
*/
import "./components/Company";
import "./components/Employee";

$(document).ready(function() {
  /*
  |  Set CSRF Token to bypass laravel authentication
  */
  $.ajaxSetup({
    headers: {
      "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
    },
  });
});
