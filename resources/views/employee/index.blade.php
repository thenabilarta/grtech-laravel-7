@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12 mt-3">
            <div class="card">
                <div class="card-header">
                    <div class="row justify-content-between align-items-center">
                        <div class="col-md-6 align-items-center">Employee Table</div>
                        <div class="col-auto">
                            <button class="btn btn-primary create-employee-data">Add Employee</button>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <table id="employee_table" class="display">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Full Name</th>
                                <th>Company</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>


        </div>
    </div>
</div>
@endsection