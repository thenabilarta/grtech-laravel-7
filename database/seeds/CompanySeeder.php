<?php

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('ms_MY');

        /*
        |  Using Faker to get dummy data
        */
        for ($i = 1; $i <= 10; $i++) {

            DB::table('companies')->insert([
                'name' => $faker->company,
                'email' => $faker->companyEmail,
                'website' => $faker->url,
                'logo' => '/uploads/apple.jpg'
            ]);
        }
    }
}
