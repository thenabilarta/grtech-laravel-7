/*
|  Set table variable
*/
var company_table;

company_table = $("#company_table").DataTable({
  processing: true,
  serverSide: true,
  order: [[0, "desc"]],
  ajax: {
    /*
    |  Fetch data from end point
    */
    url: "/company/data",
    type: "get",
  },
  columns: [
    {
      data: "id",
    },
    {
      data: "name",
    },
    {
      data: "email",
    },
    {
      /*
      |  Process the data to get the image
      */
      data: dataTableCreateLogo,
    },
    {
      /*
      |  Convert the data to a clickable link
      */
      data: dataTableCreateWebsite,
    },
    {
      data: "action",
      orderable: false,
    },
  ],
});

function dataTableCreateLogo(data) {
  const image = `<img class="company-logo" src="/storage/${data.logo}" />`;

  return image;
}

function dataTableCreateWebsite(data) {
  const link = `<a href="${data.website}">${data.website}</a>`;

  return link;
}

/*
|  Form to insert into the modal
*/
function editForm(companyData) {
  return `
    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
        <label for="input-logo-edit">
          <img class="logo-edit" src="/storage/${companyData.logo}">
        </label>
        <input type="file" name="input-logo-edit" id="input-logo-edit">
      </div>
    </div>
    <div class="form-group">
      <label for="inputName">Name</label>
      <input type="text" class="form-control" id="inputNameCompany" placeholder="Enter name" value="${companyData.name}">
    </div>
    <div class="form-group">
      <label for="inputEmail">Email</label>
      <input type="email" value="${companyData.email}" class="form-control" id="inputEmailCompany" placeholder="Enter Email">
    </div>
    <div class="form-group">
      <label for="inputWebsite">Website</label>
      <input type="website" value="${companyData.website}" class="form-control" id="inputWebsiteCompany" placeholder="Enter Website">
    </div>
    `;
}

/*
|  Global variable to detect input logo file change
*/
let editedInputLogo;

/*
|  Detect change in file input logo
*/
$(document).on("change", "#input-logo-edit", function() {
  const editLogoInput = document.querySelector("#input-logo-edit");
  const fileSource = URL.createObjectURL(editLogoInput.files[0]);
  editedInputLogo = editLogoInput.files[0];
  $(".logo-edit").attr("src", fileSource);
});

/*
|  Show edit modal for company, get data from endpoint.
|  If ok, store the data using callback
*/
$(document).on("click", ".company-action-edit", function() {
  const id = $(this)
    .attr("id")
    .split("-")[0];
  axios.get(`/company/show/${id}`).then((res) => {
    const companyData = res.data;
    var dialog = bootbox.dialog({
      title: "Edit Company",
      message: editForm(companyData),
      size: "large",
      buttons: {
        cancel: {
          label: '<i class="fa fa-times"></i> Cancel',
        },
        ok: {
          label: '<i class="fa fa-check"></i> Confirm',
          callback: function() {
            const name = $("#inputNameCompany").val();
            const email = $("#inputEmailCompany").val();
            const website = $("#inputWebsiteCompany").val();

            const logo = editedInputLogo;

            let formData = new FormData();
            formData.append("file", logo);
            formData.append("name", name);
            formData.append("email", email);
            formData.append("website", website);

            const config = {
              headers: {
                "content-type": "multipart/form-data",
              },
            };

            /*
            |  Store the data end shows notification if error or success
            */
            axios.post(`/company/${id}/store`, formData, config).then((res) => {
              if (res.data.status === "error") {
                toastr.error(res.data.message, "Error", {
                  timeOut: 2000,
                  positionClass: "toast-top-right",
                });
              }

              if (res.data.status === "success") {
                toastr.success(`Company data has been updated`, "Success", {
                  timeOut: 2000,
                  positionClass: "toast-top-right",
                });
                company_table.ajax.reload();
                dialog.modal("hide");
              }
            });

            return false;
          },
        },
      },
    });
  });
});

/*
|  Show delete modal
*/
$(document).on("click", ".company-action-delete", function() {
  const id = $(this)
    .attr("id")
    .split("-")[0];
  bootbox.dialog({
    title: "Delete media",
    message: `Do you want to delete this company? This action cannot be undone.`,
    size: "large",
    buttons: {
      cancel: {
        label: '<i class="fa fa-times"></i> Cancel',
      },
      ok: {
        label: '<i class="fa fa-check"></i> Confirm',
        callback: function() {
          axios.post(`/company/${id}/delete`).then((res) => {
            if (res.data.status === "success") {
              toastr.success(`Company data has been deleted`, "Success", {
                timeOut: 2000,
                positionClass: "toast-top-right",
              });
              company_table.ajax.reload();
            }
          });
        },
      },
    },
  });
});

/*
|  Move to create company data's page
*/
$(document).on("click", ".create-company-data", function() {
  window.location.href = "/company/create";
});
