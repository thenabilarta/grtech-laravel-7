<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    public function credits()
    {
        return $this->hasMany(Employee::class);
    }
}
