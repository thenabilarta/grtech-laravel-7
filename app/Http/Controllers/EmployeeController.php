<?php

namespace App\Http\Controllers;

use App\Company;
use App\Employee;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Notifications\EmployeeCreated;

class EmployeeController extends Controller
{
    /*
    |  Show Employee page
    */
    public function index()
    {
        return view('employee.index');
    }

    /*
    |  Show create employee's data page
    */
    public function create()
    {
        $companies = Company::all();

        return view('employee.create', compact('companies'));
    }

    /*
    |  Show 1 employee data in the modal
    */
    public function show($id)
    {
        $employee = Employee::where('id', $id)->get();

        return $employee[0];
    }

    /*
    |  Save created employee's data in create employee page
    */
    public function createSave(Request $request)
    {
        request()->validate([
            'firstName' => 'required',
            'lastName' => 'required',
            'email' => ['required', 'email'],
            'phone' => 'required',
        ]);

        $firstName = $request->firstName;
        $lastName = $request->lastName;
        $email = $request->email;
        $phone = $request->phone;
        $company_id = $request->company_id;

        if ($company_id == 0) {
            $company_id = null;
        }

        $employee = new Employee();

        $employee->first_name = $firstName;
        $employee->last_name = $lastName;
        $employee->company_id = $company_id;
        $employee->email = $email;
        $employee->phone = $phone;

        $employee->save();

        auth()->user()->notify(new EmployeeCreated("Your Employee data has been created."));

        return redirect('/employee');
    }

    /*
    |  Store edited employee data in the modal
    */
    public function store(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'data.firstName' => 'required',
            'data.lastName' => 'required',
            'data.email' => 'required|email',
            'data.phone' => 'required',
        ]);

        if ($validator->fails()) {
            $message = $validator->messages()->first();

            $message = explode(" ", $message);

            $message = array_splice($message, 1);

            $message[0] = explode(".", $message[0]);

            $message[0] = $message[0][1];

            $message[0] = ucfirst($message[0]);

            $message = implode(" ", $message);

            return response()->json(['status' => 'error', 'message' => $message]);
        }

        $firstName = $request->data['firstName'];
        $lastName = $request->data['lastName'];
        $email = $request->data['email'];
        $phone = $request->data['phone'];
        $employeesCompanyID = $request->data['employeesCompanyID'];

        if ($employeesCompanyID == 0) {
            $employeesCompanyID = null;
        }

        Employee::where('id', $id)->update(array('first_name' => $firstName, 'last_name' => $lastName, 'email' => $email, 'phone' => $phone, 'company_id' => $employeesCompanyID));

        return response()->json(['status' => 'success']);
    }

    /*
    |  Delete 1 employee's data
    */
    public function delete($id)
    {
        Employee::where('id', $id)->delete();

        return response()->json(['status' => 'success']);
    }

    /*
    |  Provide employee data for datatables
    */
    public function data(Request $request)
    {
        $employees = Employee::all();

        foreach ($employees as $e) {
            if (!$e['company_id']) {
                $e["company_name"] = "";
                continue;
            }

            $company_name = Company::where('id', $e['company_id'])->get();
            $e["company_name"] = $company_name[0]['name'];
        }

        // Convert data to eloquent so datatables can read the data
        $collection = new EloquentCollection();

        foreach ($employees as $e) {
            $collection->push((object)[
                'id' => $e['id'],
                'full_name' => $e['first_name'] . " " . $e['last_name'],
                'company_id' => $e['company_id'],
                'company_name' => $e['company_name'],
                'email' => $e['email'],
                'phone' => $e['phone'],
            ]);
        }

        if ($request->ajax()) {
            return datatables()->of($collection)
                ->addColumn('action', function ($data) {
                    $button = '<div class="dropdown">
                <button class="btn dropdown-toggle" type="button" data-toggle="dropdown">
                  <span class="caret"></span></button>
                <ul class="dropdown-menu dropdown-menu-right">
                    <span class="dropdown-item employee-action-edit" id="' . $data->id . '-company-action">Edit</span>
                    <span class="dropdown-item employee-action-delete" id="' . $data->id . '-company-action">Delete</span>
                </ul>
              </div>';
                    return $button;
                })
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }
    }
}
