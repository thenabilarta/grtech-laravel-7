<?php

namespace App\Http\Controllers;

use App\Company;
use App\Employee;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CompanyController extends Controller
{
    /*
    |  Show company page
    */
    public function index()
    {
        return view('company.index');
    }

    /*
    |  Show create company's page
    */
    public function create()
    {
        return view('company.create');
    }

    /*
    |  Show 1 Company's data
    */
    public function show($id)
    {
        $company = Company::where('id', $id)->get();

        return $company[0];
    }

    /*
    |  Save created company's data
    */
    public function createSave(Request $request)
    {
        request()->validate([
            'image' => ['required', 'mimes:jpeg,jpg,png'],
            'name' => 'required',
            'email' => ['required', 'email'],
            'website' => ['required', 'url'],
        ]);

        $logo = $request->image;

        $imagePath = $logo->store('uploads', 'public');

        $name = $request->name;
        $email = $request->email;
        $website = $request->website;

        $company = new Company();

        $company->name = $name;
        $company->email = $email;
        $company->website = $website;
        $company->logo = $imagePath;

        $company->save();

        return redirect('/company');
    }

    /*
    |  Save edited company's data from modal
    */
    public function store(Request $request, $id)
    {
        if ($request->file != 'undefined') {
            $validator = Validator::make($request->all(), [
                'file' => ['mimes:jpeg,jpg,png'],
                'name' => 'required',
                'email' => 'required|email',
                'website' => 'required|url',
            ]);

            if ($validator->fails()) {
                $message = $validator->messages()->first();

                return response()->json(['status' => 'error', 'message' => $message]);
            }

            $logo = $request->file;

            $imagePath = $logo->store('uploads', 'public');

            $name = $request->name;
            $email = $request->email;
            $website = $request->website;

            Company::where('id', $id)->update(array('name' => $name, 'email' => $email, 'website' => $website, 'logo' => $imagePath));

            return response()->json(['status' => 'success']);
        }

        if ($request->file == 'undefined') {
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'email' => 'required|email',
                'website' => 'required|url',
            ]);

            if ($validator->fails()) {
                $message = $validator->messages()->first();

                return response()->json(['status' => 'error', 'message' => $message]);
            }

            $name = $request->name;
            $email = $request->email;
            $website = $request->website;

            Company::where('id', $id)->update(array('name' => $name, 'email' => $email, 'website' => $website));

            return response()->json(['status' => 'success']);
        }
    }

    /*
    |  Delete 1 company's data
    */
    public function delete($id)
    {
        Company::where('id', $id)->delete();

        Employee::where('company_id', $id)->update(array('company_id' => null));

        return response()->json(['status' => 'success']);
    }

    /*
    |  Show list of company for employee's page to show company's employee works for
    */
    public function list()
    {
        $companies = Company::all();

        return $companies;
    }

    /*
    |  Provide list of company data for datatables in company page
    */
    public function data(Request $request)
    {
        $companies = Company::all();

        // Convert data to eloquent so datatables can read the data
        $collection = new EloquentCollection();

        foreach ($companies as $c) {
            $collection->push((object)[
                'id' => $c['id'],
                'name' => $c['name'],
                'email' => $c['email'],
                'logo' => $c['logo'],
                'website' => $c['website'],
            ]);
        }

        if ($request->ajax()) {
            return datatables()->of($collection)
                ->addColumn('action', function ($data) {
                    $button = '<div class="dropdown">
                <button class="btn dropdown-toggle" type="button" data-toggle="dropdown">
                  <span class="caret"></span></button>
                <ul class="dropdown-menu dropdown-menu-right">
                    <span class="dropdown-item company-action-edit" id="' . $data->id . '-company-action">Edit</span>
                    <span class="dropdown-item company-action-delete" id="' . $data->id . '-company-action">Delete</span>
                </ul>
              </div>';
                    return $button;
                })
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }
    }
}
